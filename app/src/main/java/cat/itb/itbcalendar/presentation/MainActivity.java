package cat.itb.itbcalendar.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cat.itb.itbcalendar.R;
import cat.itb.itbcalendar.presentation.screens.dayofweek.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow();
        }
    }
}
